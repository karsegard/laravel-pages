<?php

namespace KDA\Pages\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use KDA\Pages\Models\Relations\Pageblock;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'content_id',
        'content_type',
        'meta',
        'meta->default'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
        'id'=>'integer',
        'meta'=>'array',
        'meta->default'=>'boolean'
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Pages\Database\Factories\PageFactory::new();
    }

    public function content(){
        return $this->morphTo();
    }


    public function blocks(){
        return $this->hasMany(Pageblock::class);
    }

    public function getRelatedModelsAttribute()
    {
        return $this->blocks->groupBy('pageblock_type');
    }
    public function getSortedModelsAttribute()
    {
        return $this->blocks;
    }
}
