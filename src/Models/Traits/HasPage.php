<?php

namespace KDA\Pages\Models\Traits;

use KDA\Pages\Models\Page;

use KDA\Pages\Models\Relations\Pageblock;

trait HasPage
{


    public static function bootHasPage()
    {
        
    }


    public function page()
    {
        return $this->morphOne(Page::class, 'content');
    }
    public function pages()
    {
        return $this->morphToMany(Page::class, 'pageblock')
                    ->using(Pageblock::class);
    }
    public function scopeWithNoPage($query)
    {
        return $query->whereDoesntHave('page');
    }
}
