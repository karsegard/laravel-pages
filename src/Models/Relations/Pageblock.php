<?php
namespace KDA\Pages\Models\Relations;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

use KDA\Pages\Models\Page;
class Pageblock extends MorphPivot
{
    protected $table = 'pageblocks';

    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function block()
    {
        return $this->belongsTo(Page::class);
    }

    public function related()
    {
        return $this->morphTo(__FUNCTION__, 'pageblock_type', 'pageblock_id');
    }
}