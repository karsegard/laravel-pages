<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Post extends Model 
{
   
    use \KDA\Pages\Models\Traits\HasPage;
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    
    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
