<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Pages\Models\Page;
use KDA\Tests\Models\Post;

use KDA\Tests\TestCase;
use DB;
class PostTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function a_post_has_page()
  {
    $o = Page::factory()->create(['name' => 'Fake Title']);

    $p = Post::factory()->create(['title'=>'test']);

    
    $o->content()->associate($p)->save();
    $this->assertEquals($o->id, $p->page->id);

  }

  /** @test */

  function page_can_have_posts()
  {
   
    $o = Page::factory()->create(['name' => 'Fake Title']);

    $p = Post::factory()->create(['title'=>'test']);

    
    $p->pages()->attach($p);
  //$this->assertEquals($o->id, $p->page->id);
    //dump($o->fresh()->load('blocks')->toArray());

    $count = 0;
    DB::listen(function ($query) use (&$count) {
        $count++;
    });

    $pages = Page::all();
    dump($pages->map(function($p){
      return $p->relatedModels;
    }));
    dump($pages->map(function($p){
      return $p->sortedModels;
    }));
    //dump($o->blocks[0]->related);
    dump($count);
  }
  /** @test */
/* function a_slug_has_a_collection()
  {
    $c = SlugCollection::factory()->create(['name'=>'Pages']);
    $o = Slug::factory()->create(['slug' => 'Fake Title','collection_id'=>$c->id]);
    $this->assertEquals('Fake Title', $o->slug);
    $this->assertEquals($c->id, $o->collection->id);

  }
*/


  
}