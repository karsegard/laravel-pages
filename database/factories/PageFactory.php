<?php

namespace KDA\Pages\Database\Factories;

use KDA\Pages\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;

class PageFactory extends Factory
{
    protected $model = Page::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
