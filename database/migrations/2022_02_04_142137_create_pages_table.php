<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('meta')->nullable();
            $table->nullableNumericMorphs('content');
            $table->timestamps();
        });

        Schema::create('pageblocks', function (Blueprint $table) {
            $table->unsignedBigInteger('page_id');
            $table->numericMorphs('pageblock');
            $table->integer('sort')->nullable();
        });

        /**
         * id | template | data | lft | rgt | depth | section_type | section_id
         * 1    site.page   []    1    2    0   
         * 
         */
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pages');
        Schema::dropIfExists('pageblocks');
        Schema::enableForeignKeyConstraints();
    }
}
